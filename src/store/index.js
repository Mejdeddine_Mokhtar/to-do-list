import { createStore } from "vuex";

const store = createStore({

    state:{
        todos:[]
    },

    mutations:{
        addTodo(state, payload){
            state.todos.push(payload)
        },
        deleteTodo(state, payload){
            state.todos.splice(payload.id, 1)
        }


    }
})


export default store;